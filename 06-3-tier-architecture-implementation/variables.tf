variable "vpc_cidr" {
  type        = string
  description = "vpc network cidr block for malc vpc"

}

variable "public_subnet_cidr" {
  type        = list(any)
  description = "list of public subnet"

}

variable "private_subnet_cidr" {
  type        = list(any)
  description = "list of private subnet"

}

variable "database_subnet_cidr" {
  type        = list(any)
  description = "list of datbase subnets"
}

variable "public_instance_type" {
  type        = string
  description = "instance type for public instance"
  default     = "t2.micro"

}

variable "private_instance_type" {
  type        = string
  description = "instance type for private instance"
  default     = "t2.micro"

}

variable "registrationapp_instance_type" {
  type        = string
  description = "registration app instance type"
  default     = "t2.xlarge"
}

variable "db_name" {
  type        = string
  description = "value for database name"
  default     = "webappdb"

}

variable "instance_class" {
  type        = string
  description = "instance class for database"
  default     = "db.t2.micro"

}

variable "db_username" {
  type        = string
  description = "database username"
  default     = "malcnwai"

}

variable "db_port" {
  type        = number
  description = "database port"
  default     = 3306

}

variable "dns_name" {
  type        = string
  description = "This is the dns name"

}

variable "subject_alternative_names" {
  type = list(any)

}