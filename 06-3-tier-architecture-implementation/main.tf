
locals {
  vpc_id = aws_vpc.this.id
  az     = data.aws_availability_zones.available.names
}

############################################################
# CREATING VPC
############################################################

resource "aws_vpc" "this" {
  cidr_block           = var.vpc_cidr
  enable_dns_support   = true
  enable_dns_hostnames = true

  tags = {
    Name = "malc-vpc-${terraform.workspace}"
  }
}

############################################################
# CREATING INTERNET GATEWAY
############################################################

resource "aws_internet_gateway" "igw" {
  vpc_id = local.vpc_id

  tags = {
    Name = "malc-igw-${terraform.workspace}"
  }
}

############################################################
# CREATING PUBLIC SUBNET
############################################################

resource "aws_subnet" "public_subnet" {
  count                   = length(var.public_subnet_cidr)
  vpc_id                  = local.vpc_id
  cidr_block              = var.public_subnet_cidr[count.index]
  availability_zone       = local.az[count.index] # element
  map_public_ip_on_launch = true

  tags = {
    Name = "public_subnet-${count.index + 1}-${terraform.workspace}"
  }
}

############################################################
# CREATING PRIVATE SUBNET
############################################################

resource "aws_subnet" "private_subnet" {
  count             = length(var.private_subnet_cidr)
  vpc_id            = local.vpc_id
  cidr_block        = var.private_subnet_cidr[count.index]
  availability_zone = local.az[count.index]

  tags = {
    Name = "private_subnet-${count.index + 1}-${terraform.workspace}"
  }
}

############################################################
# CREATING DATABASE SUBNET
############################################################

resource "aws_subnet" "db_subnet" {
  count             = 2
  vpc_id            = local.vpc_id
  cidr_block        = var.database_subnet_cidr[count.index]
  availability_zone = "us-east-1a"

  tags = {
    Name = "db_subnet-${count.index + 1}-${terraform.workspace}"
  }
}


############################################################
# CREATING PUBLIC ROUTE TABLE
############################################################

resource "aws_route_table" "public_route_table" {
  vpc_id = local.vpc_id

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.igw.id
  }


  tags = {
    Name = "public_route_table-${terraform.workspace}"
  }
}

############################################################
# CREATING PUBLIC ROUTE TABLE ASSOCIATION WITH PUBLIC SUBNET
############################################################

resource "aws_route_table_association" "rt_association" {
  count          = length(var.public_subnet_cidr)
  subnet_id      = aws_subnet.public_subnet.*.id[count.index]
  route_table_id = aws_route_table.public_route_table.id
}

############################################################
# CREATING DEFAULT ROUTE TABLE FOR PRIVATE SUBNETS
############################################################

resource "aws_default_route_table" "this" {
  default_route_table_id = aws_vpc.this.default_route_table_id

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_nat_gateway.this.id
  }

  tags = {
    Name = "this"
  }
}

############################################################
# CREATING NAT GATEWAY
############################################################

resource "aws_nat_gateway" "this" {
  depends_on    = [aws_internet_gateway.igw]
  allocation_id = aws_eip.eip.id
  subnet_id     = aws_subnet.public_subnet[0].id

  tags = {
    Name = "this"
  }
}

############################################################
# CREATING EIP
############################################################

resource "aws_eip" "eip" {
  depends_on = [aws_internet_gateway.igw]
  vpc        = true
}