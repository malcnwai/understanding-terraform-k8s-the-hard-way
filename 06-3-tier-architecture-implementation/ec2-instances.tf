
################################
# CREATING app1
################################

resource "aws_instance" "app1" {
  ami                  = data.aws_ami.ami.id
  instance_type        = var.private_instance_type
  iam_instance_profile = aws_iam_instance_profile.instance_profile.name
  subnet_id            = aws_subnet.private_subnet[0].id
  user_data            = file("${path.module}/templates/app1.sh")
  security_groups      = [aws_security_group.static_sg.id] #80 from ALB


  tags = {
    Name = "app1-${terraform.workspace}"
  }
}

################################
# CREATING app2
################################

resource "aws_instance" "app2" {
  ami                  = data.aws_ami.ami.id
  instance_type        = var.private_instance_type
  subnet_id            = aws_subnet.private_subnet[1].id
  iam_instance_profile = aws_iam_instance_profile.instance_profile.name
  user_data            = file("${path.module}/templates/app2.sh")
  security_groups      = [aws_security_group.static_sg.id] #80 from ALB

  tags = {
    Name = "app2-${terraform.workspace}"
  }
}

################################
# CREATING REGRISTRATION APP.
################################

resource "aws_instance" "registration_app" {
  depends_on = [aws_db_instance.registration_app_db, aws_secretsmanager_secret_version.this]

  count                = 2
  ami                  = data.aws_ami.ami.id
  instance_type        = var.registrationapp_instance_type
  subnet_id            = [aws_subnet.private_subnet[0].id, aws_subnet.private_subnet[1].id][count.index]
  iam_instance_profile = aws_iam_instance_profile.instance_profile.name
  user_data = templatefile("${path.module}/templates/registration_app.tmpl",
    {
      application_version = "v1.2.0"
      hostname            = "${aws_db_instance.registration_app_db.address}"
      port                = "${var.db_port}"
      db_name             = "${var.db_name}"
      db_username         = "${var.db_username}"
      db_password         = "${random_password.password.result}"


    }
  )
  security_groups = [aws_security_group.registration_app_sg.id] #8080 from ALB

  tags = {
    Name = "registration-app-${terraform.workspace}"
  }
}