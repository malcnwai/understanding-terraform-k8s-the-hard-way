
############################################################
# CREATING SECURITY GROUP FOR Load balancer
############################################################
resource "aws_security_group" "alb_sg" {
  name        = "alb-sg-${terraform.workspace}"
  description = "Allow traffic from on port 80"
  vpc_id      = local.vpc_id

  ingress {
    description = "Allow ingress traffic on port 80"
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    description = "Allow ingress traffic on port 443"
    from_port   = 443
    to_port     = 443
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Name = "alb-sg-${terraform.workspace}"
  }
}

############################################################
# CREATING SECURITY GROUP FOR web application
############################################################

resource "aws_security_group" "static_sg" {
  name        = "static_sg-${terraform.workspace}"
  description = "Allow inbound traffic from alb security group id"
  vpc_id      = local.vpc_id

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Name = "static-sg-${terraform.workspace}"
  }
}

############################################################
# CREATING SECURITY GROUP RULE FOR static sg
############################################################

resource "aws_security_group_rule" "Allow_ssh_from_bastion_sg" {
  security_group_id        = aws_security_group.static_sg.id
  type                     = "ingress"
  from_port                = 80
  to_port                  = 80
  protocol                 = "tcp"
  source_security_group_id = aws_security_group.alb_sg.id
}

############################################################
# CREATING SECURITY GROUP FOR REGRISTRATION APP
############################################################

resource "aws_security_group" "registration_app_sg" {
  name        = "registration_app_sg-${terraform.workspace}"
  description = "Allow inbound traffic from alb security group id"
  vpc_id      = local.vpc_id

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Name = "registration_app-sg-${terraform.workspace}"
  }
}

############################################################
# CREATING SECURITY GROUP RULE FOR REGISTRATION APP sg
############################################################

resource "aws_security_group_rule" "registration_sg" {
  security_group_id        = aws_security_group.registration_app_sg.id
  type                     = "ingress"
  from_port                = 8080
  to_port                  = 8080
  protocol                 = "tcp"
  source_security_group_id = aws_security_group.alb_sg.id
}

############################################################
# CREATING SECURITY GROUP FOR Database A
############################################################

resource "aws_security_group" "database_sg" {
  name        = "my-sql_sg-${terraform.workspace}"
  description = "Allow inbound traffic from registration app security group"
  vpc_id      = local.vpc_id

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Name = "my-sql-sg-${terraform.workspace}"
  }
}

############################################################
# CREATING SECURITY GROUP RULE FOR REGISTRATION APP sg
############################################################

resource "aws_security_group_rule" "my-sql-inbound_sg" {
  security_group_id        = aws_security_group.database_sg.id
  type                     = "ingress"
  from_port                = 3306
  to_port                  = 3306
  protocol                 = "tcp"
  source_security_group_id = aws_security_group.registration_app_sg.id
}