Building a highly available 3-tier application on AWS using terraform end-to-end.

Procedures(Step-by-Step):
1. Create repo/Use existing repo
2. Configure backend (new s3 bucket and dynamo db)
3. Configure backend in terraform.tf
3. Create various tf files
4. Create a workspace
- terraform init
- terraform workspace new sbx
- terraform workspace list
5. Create workspace tfvars
6. Run terraform fmt/validate/plan/apply/destory
- terraform fmt
- terraform validate
- terraform plan -var-file sbx.tfvars