data "aws_availability_zones" "available" {
  state = "available"
}

data "aws_ami" "ami" {
  most_recent = true
  owners      = ["amazon"]

  filter {
    name   = "name"
    values = ["amzn2-ami-kernel-5.10-hvm*"]
  }

  filter {
    name   = "root-device-type"
    values = ["ebs"]
  }
}

# data "aws_secretsmanager_secret_version" "this" {
#   depends_on = [
#     aws_secretsmanager_secret.this
#   ]
#   secret_id = aws_secretsmanager_secret.this.id
# }