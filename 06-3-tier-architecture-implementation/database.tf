
################################
# CREATING Database.
################################

locals {
  db_credentials = {
    endpoint    = aws_db_instance.registration_app_db.address
    db_username = var.db_username
    db_name     = var.db_name
    port        = var.db_port
    password    = random_password.password.result
  }
}

resource "aws_secretsmanager_secret" "this" {
  name        = "malc-mysql-secret-${terraform.workspace}"
  description = "Secret to manage mysql superuser password"
  recovery_window_in_days = 0
}



resource "aws_secretsmanager_secret_version" "this" {
  secret_id     = aws_secretsmanager_secret.this.id
  secret_string = jsonencode(local.db_credentials)
}

resource "random_password" "password" {
  length  = 16
  special = false
}

resource "aws_db_subnet_group" "mysql_subnet-group" {
  name       = "registrationapp_subnet_group-${terraform.workspace}"
  subnet_ids = ([aws_subnet.private_subnet[0].id, aws_subnet.private_subnet[1].id])

  tags = {
    Name = "registrationapp_subnet_group-${terraform.workspace}"
  }
}


resource "aws_db_instance" "registration_app_db" {
  allocated_storage      = 20
  db_name                = var.db_name #application team will provide this (webappdb)
  engine                 = "mysql"
  instance_class         = var.instance_class
  username               = var.db_username
  password               = random_password.password.result
  port                   = var.db_port #3306
  db_subnet_group_name   = aws_db_subnet_group.mysql_subnet-group.name
  vpc_security_group_ids = [aws_security_group.database_sg.id]
  skip_final_snapshot    = true
}