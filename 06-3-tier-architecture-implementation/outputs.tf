output "app1" {
  value = aws_instance.app1.id
}

output "app2" {
  value = aws_instance.app2.id
}

# output "secrets" {
#   value = jsondecode(data.aws_secretsmanager_secret_version.this.secret_string)["key1"]
# }

output "registration_app" {
  value = aws_instance.registration_app.*.id
}