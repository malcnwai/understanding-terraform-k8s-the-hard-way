resource "aws_vpc" "vpc-practice" {
  cidr_block = var.vpc-cidr

  tags = {
    Name = "resource-group-vpc"
  }
}

// #########################
//       PRIVATE SUBNET
// #########################

variable "private_subnets" {
  default = {
    private_subnet_1 = {
      cidr_block        = "10.0.2.0/24"
      # availability_zone = data.aws_availability_zones.available.names
    }
    private_subnet_2 = {
      cidr_block        = "10.0.4.0/24"
      # availability_zone = data.aws_availability_zones.available.names
    }
  }
}

resource "aws_subnet" "Resource-private-subnet" {
  for_each          = var.private_subnets
  vpc_id            = local.vpc_cidr
  cidr_block        = each.value.cidr_block
  # availability_zone = each.value.availability_zone

  tags = {
    Name = each.key
  }
}

# resource "aws_subnet" "Resource-private-subnet" {
#   count             = length(var.priv-sn-cidr)
#   vpc_id            = local.vpc_cidr
#   cidr_block        = var.priv-sn-cidr[count.index]
#   availability_zone = data.aws_availability_zones.available.names[count.index]

#   tags = {
#     Name = "Resource-private-subnet-${count.index + 1}"
#   }
# }

// #########################
//       PUBLIC SUBNET
// #########################

variable "public_subnets" {
  default = {
    public_subnet_1 = {
      cidr_block        = "10.0.1.0/24"
      # availability_zone = data.aws_availability_zones.available.names
    }
    public_subnet_2 = {
      cidr_block        = "10.0.3.0/24"
      # availability_zone = data.aws_availability_zones.available.names
    }
  }
}

resource "aws_subnet" "Resource-public-subnet" {
  for_each          = var.public_subnets
  vpc_id            = local.vpc_cidr
  cidr_block        = each.value.cidr_block
  # availability_zone = each.value.availability_zone

  tags = {
    Name = each.key
  }
}

# resource "aws_subnet" "Resource-Public-subnet-1" {
#   count                   = length(var.pub-sn-cidr)
#   vpc_id                  = local.vpc_cidr
#   cidr_block              = var.pub-sn-cidr[count.index]
#   availability_zone       = data.aws_availability_zones.available.names[count.index]
#   map_public_ip_on_launch = true

#   tags = {
#     Name = "Resource-Public-subnet-${count.index + 1}"
#   }
