variable "vpc-cidr" {
  type        = string
  description = "vpc cidr"
  default     = "10.0.0.0/16"
}

variable "priv-sn-cidr" {
  type        = list(any)
  description = "cidr blocks for private subnet"
  default     = ["10.0.2.0/24", "10.0.4.0/24"]

}

variable "pub-sn-cidr" {
  type        = list(any)
  description = "cidr blocks for public subnet"
  default     = ["10.0.1.0/24", "10.0.3.0/24"]

}

