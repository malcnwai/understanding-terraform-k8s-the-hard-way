# locals {
#    vpc_id = aws_vpc.aws_vpc.id

# }



locals {
  vpc_id = aws_vpc.for_each.id

}

locals {
  public_subnet = {
    public-subnet-1 = {
      cidr_block        = "10.0.0.0/24"
      availability_zone = "us-east-1a"
    }
    public-subnet-2 = {
      cidr_block        = "10.0.2.0/24"
      availability_zone = "us-east-1b"
    }
    public-subnet-3 = {
      cidr_block        = "10.0.4.0/24"
      availability_zone = "us-east-1c"
    }
    public-subnet-4 = {
      cidr_block        = "10.0.6.0/24"
      availability_zone = "us-east-1d"
    }
  }
}
