variable "vpc_cidr_block" {
  type        = string
  description = "vpc cidr block"
  default     = "10.0.0.0/16"
}

variable "public_subnet_cidr" {
  description = "value for public subnet cidr"
  type        = list(any)
  default     = ["10.0.0.0/24", "10.0.2.0/24", "10.0.4.0/24", "10.0.6.0/24"]

}

variable "private_subnet_cidr" {
  description = "value for private subnet cidr"
  type        = list(any)
  default     = ["10.0.7.0/24", "10.0.1.0/24", "10.0.3.0/24", "10.0.5.0/24"]

}

variable "nat_gateway" {
  description = "value"
  type        = bool
  default     = true

}

variable "vpn_gateway" {
  description = "value"
  type        = bool
  default     = true

}
/*
variable "sn1_az" {
  description = "az for subnet 1"
  type        = string
  default     = "us-east-1a"

}

variable "sn2_az" {
  description = "az for subnet 2"
  type        = string
  default     = "us-east-1b"

}
*/