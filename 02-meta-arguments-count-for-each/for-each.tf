###############
# FOR-EACH FOR META-ARGUMENTS
###############


# Create a VPC
resource "aws_vpc" "for_each" {
  cidr_block = var.vpc_cidr_block

  tags = {
    Name = "terraform-vpc-1"
  }
}

# locals {
#   public_subnet = {
#     public-subnet-1 = {
#       cidr_block        = "10.0.0.0/24"
#       availability_zone = "us-east-1a"
#     }
#     public-subnet-2 = {
#       cidr_block        = "10.0.2.0/24"
#       availability_zone = "us-east-1b"
#     }
#     public-subnet-3 = {
#       cidr_block        = "10.0.4.0/24"
#       availability_zone = "us-east-1c"
#     }
#     public-subnet-4 = {
#       cidr_block        = "10.0.6.0/24"
#       availability_zone = "us-east-1d"
#     }
#   }
# }

## Create Subnet and use data source
# resource "aws_subnet" "public_subnets_for_each" {
#   for_each = local.public_subnet

#   vpc_id                  = local.vpc_id
#   cidr_block              = each.value.cidr_block
#   availability_zone       = each.value.availability_zone
#   map_public_ip_on_launch = true

#   tags = {
#     Name = each.key
#   }
# }

variable "private_subnets" {
  type        = map
  description = "objects of public_subnets to be created"
  default = {
    private-subnet-1 = {
      cidr_block        = "10.0.1.0/24"
      availability_zone = "us-east-1a"
    }
     private-subnet-2 = {
      cidr_block        = "10.0.3.0/24"
      availability_zone = "us-east-1b"
    }
  }

}


resource "aws_subnet" "private_subnets_for_each" {
  for_each          = var.private_subnets
  vpc_id            = local.vpc_id
  cidr_block        = each.value.cidr_block
  availability_zone = each.value.availability_zone


  tags = {
    Name = each.key
  }
}

# resource "aws_instance" "web_for_each" {
#   ami           = data.aws_ami.instance_ami.id
#   instance_type = "t2.micro"
# subnet_id = aws_subnet.public_subnets_for_each[1].id

#   tags = {
#     Name = "tf-web-instance"
#   }
# }

