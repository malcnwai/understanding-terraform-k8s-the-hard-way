# ###
# # Root Modules
# ####

# ### Private Modules / Public Modules

# module "vpc" {
#   source = "terraform-aws-modules/vpc/aws"

#   name = "my-vpc"
#   cidr = "10.0.0.0/16"

#   azs             = ["eu-west-1a", "eu-west-1b", "eu-west-1c"] # use data source
#   private_subnets = ["10.0.1.0/24", "10.0.2.0/24", "10.0.3.0/24"]
#   public_subnets  = ["10.0.101.0/24", "10.0.102.0/24", "10.0.103.0/24"]

#   enable_nat_gateway = var.nat_gateway #use variable
#   enable_vpn_gateway = var.vpn_gateway #use variable

#   tags = {
#     Terraform = "true"
#     Environment = "dev"
#   }
# }