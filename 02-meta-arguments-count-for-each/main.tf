# ###
# # COUNT FOR META-ARGUMENTS
# ####

# # Create a VPC
# resource "aws_vpc" "aws_vpc" {
#   cidr_block = var.vpc_cidr_block

#   tags = {
#     Name = "terraform-vpc-1"
#   }
# }

# ## Create Subnet and use data source
# resource "aws_subnet" "public_subnets" {
#   count                   = length(var.public_subnet_cidr)
#   vpc_id                  = local.vpc_id
#   cidr_block              = var.public_subnet_cidr[count.index]
#   availability_zone       = data.aws_availability_zones.available.names[count.index] #ToDo (element)
#   map_public_ip_on_launch = true

#   tags = {
#     Name = "public-subnet-${count.index + 1}" 
#   }
# }

# # Splat operator / Legacy operator  outputs for Lists
# # aws_subnet.public_subnets[*].id
# # aws_subnet.public_subnets.*.id

# resource "aws_subnet" "private_subnets" {
#   count             = 3
#   vpc_id            = local.vpc_id
#   cidr_block        = var.private_subnet_cidr[count.index]
#   availability_zone = data.aws_availability_zones.available.names[count.index]


#   tags = {
#     Name = "private_subnets-${count.index + 1}"
#   }
# }

# resource "aws_instance" "tf-instance_1" {
#   ami           = data.aws_ami.instance_ami.id
#   instance_type = "t2.micro"
# subnet_id = aws_subnet.public_subnets[1].id

#   tags = {
#     Name = "tf_instance_1"
#   }
# }

