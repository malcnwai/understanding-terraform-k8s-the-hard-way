

data "aws_ami" "ami" {
  most_recent = true
  owners      = ["amazon"]

  filter {
    name   = "name"
    values = ["amzn2-ami-kernel-5.10-hvm-*"]
  }

  filter {
    name   = "root-device-type"
    values = ["ebs"]
  }
}

# module "vpc" {
#   source = "terraform-aws-modules/vpc/aws"

#   name = "module-vpc"
#   cidr = "10.0.0.0/16"

#   azs             = ["us-east-1a", "us-east-1b"]
#   private_subnets = ["10.0.1.0/24", "10.0.2.0/24"]
#   public_subnets  = ["10.0.101.0/24", "10.0.102.0/24"]

#   enable_nat_gateway = true # variable(bool)
#   enable_vpn_gateway = true # variable(bool)

#   tags = {
#     Terraform   = "true"
#     Environment = "dev"
#   }
# }

# ################################################################################
# # CREATING SECURITY GROUP FOR BASTON SERVER.
# ################################################################################

resource "aws_security_group" "jump-server" {
  name        = "jump-server"
  description = "Allow traffic on port 80"
  vpc_id      = var.vpc_id

  ingress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Name = "jump-server"
  }
}

################################################################################
# CREATING PUBLIC EC2 INSTANCE.
################################################################################

# resource "aws_instance" "jump-server" {

#   ami                    = data.aws_ami.ami.id
#   instance_type          = "t2.micro"
#   subnet_id              = module.vpc.public_subnets[0]
#   iam_instance_profile   = aws_iam_instance_profile.instance_profile.name
#   vpc_security_group_ids = [aws_security_group.jump-server.id]
#   user_data              = file("./templates/jump-server.sh")
#   tags = {
#     Name = "jump-server"
#   }

# }

resource "aws_instance" "jump-server" {

  ami                    = data.aws_ami.ami.id
  instance_type          = "t2.micro"
  subnet_id              = var.subnet_id
  iam_instance_profile   = aws_iam_instance_profile.instance_profile.name
  vpc_security_group_ids = [aws_security_group.jump-server.id]
  user_data              = file("./templates/jump-server.sh")
  tags = {
    Name = "jump-server"
  }

}