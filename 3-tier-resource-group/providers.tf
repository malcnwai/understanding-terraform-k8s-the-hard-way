provider "aws" {

  region = "us-east-1"
  # Using one aws account for different environments (dev, sandbox, prod)
}