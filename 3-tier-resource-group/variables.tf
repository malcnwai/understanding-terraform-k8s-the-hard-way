variable "resource-vpc-cidr" {
  type        = string
  description = "vpc cidr"
  default     = "10.0.0.0/16"

}