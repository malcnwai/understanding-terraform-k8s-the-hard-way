############################
## Create VPC
############################


resource "aws_vpc" "resource-vpc" {
  cidr_block = var.resource-vpc-cidr

  tags = {
    Name = "resource-vpc"
  }
}

############################################
## Create Internet Gateway
############################################

resource "aws_internet_gateway" "igw" {
  vpc_id = aws_vpc.resource-vpc.id

  tags = {
    Name = "resource-igw"
  }
}

############################################
## Create Subnets
############################################

variable "public_subnets" {
  default = {
    public_subnet_1 = {
      cidr_block = "10.0.2.0/24"
      # availability_zone = data.aws_availability_zones.available.names
    }
    public_subnet_2 = {
      cidr_block = "10.0.4.0/24"
      # availability_zone = data.aws_availability_zones.available.names
    }
  }
}

resource "aws_subnet" "resource-public-subnet" {
  for_each   = var.public_subnets
  vpc_id     = aws_vpc.resource-vpc.id
  cidr_block = each.value.cidr_block

  tags = {
    Name = each.key
  }
}

variable "private_subnets" {
  default = {
    private_subnet_1 = {
      cidr_block = "10.0.1.0/24"
      # availability_zone = data.aws_availability_zones.available.names
    }
    private_subnet_2 = {
      cidr_block = "10.0.3.0/24"
      # availability_zone = data.aws_availability_zones.available.names
    }
  }
}

resource "aws_subnet" "resource-private-subnet" {
  for_each   = var.private_subnets
  vpc_id     = aws_vpc.resource-vpc.id
  cidr_block = each.value.cidr_block

  tags = {
    Name = each.key
  }
}