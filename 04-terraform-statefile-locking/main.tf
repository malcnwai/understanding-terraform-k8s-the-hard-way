resource "aws_s3_bucket" "bucket" {
  count  = length(var.bucket-name)
  bucket = var.bucket-name[count.index]

  lifecycle {
    prevent_destroy = false

  }
}

# resource "aws_s3_bucket_acl" "this" {
#   bucket = aws_s3_bucket.this.*.id
#   acl    = "private"
# }

resource "aws_dynamodb_table" "dynamodb-terraform-lock" {
  name           = "terraform-lock"
  hash_key       = "LockID"
  read_capacity  = 20
  write_capacity = 20


  attribute {
    name = "LockID"
    type = "S"
  }


  lifecycle {
    prevent_destroy = true

  }
}