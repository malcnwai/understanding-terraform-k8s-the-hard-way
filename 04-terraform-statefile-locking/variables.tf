variable "region" {
  description = "region for the provider"
  type        = string
  default     = "us-east-1"

}

variable "bucket-name" {
  description = "s3 bucket name"
  type        = list(any)
  default     = ["class-state-bucket", 
  "resource-group-state-bucket", 
  "workspace.state.bucket", 
  "mn-3-tier-state-bucket"]

}