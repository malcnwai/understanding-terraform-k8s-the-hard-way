data "aws_ami" "ami" {
  most_recent = true
  owners      = ["amazon"]

  filter {
    name   = "name"
    values = ["amzn2-ami-kernel-5.10-hvm*"]
  }

  filter {
    name   = "root-device-type"
    values = ["ebs"]
  }
}

data "aws_key_pair" "my_keypair" {
  key_name = var.key_name

}

data "aws_availability_zones" "available" {
  state = "available"
}