# ##### Create 2 EC2 intances making use of for_each block

# # # Create a VPC
# resource "aws_vpc" "aws_vpc" {
#   cidr_block = var.vpc_cidr_block

#   tags = {
#     Name = "terraform-vpc-1"
#   }
# }

# locals {
#   vpc_id = aws_vpc.aws_vpc.id
# }

# variable "public_subnet" {
#   default = {
#     public-subnet-1 = {
#       cidr_block        = "10.0.0.0/24"
#       availability_zone = "us-east-1a"
#     }
#     public-subnet-2 = {
#       cidr_block        = "10.0.2.0/24"
#       availability_zone = "us-east-1b"
#     }

#   }
# }

# ## Create Subnet and use data source
# resource "aws_subnet" "public_subnets" {
#   for_each                = var.public_subnet
#   vpc_id                  = local.vpc_id
#   cidr_block              = each.value.cidr_block
#   availability_zone       = each.value.availability_zone
#   map_public_ip_on_launch = true

#   tags = {
#     Name = each.key
#   }
# }

# # Splat operator / Legacy operator  outputs for Lists
# # aws_subnet.public_subnets[*].id
# # aws_subnet.public_subnets.*.id

# # resource "aws_subnet" "private_subnets" {
# #   count             = 3
# #   vpc_id            = local.vpc_id
# #   cidr_block        = var.private_subnet_cidr[count.index]
# #   availability_zone = data.aws_availability_zones.available.names[count.index]


# #   tags = {
# #     Name = "private_subnets-${count.index + 1}"
# #   }
# # }

# variable "public_app" {
#   description = "value for public subnet ids"
#   default = {
#     web-app-1 = {
#       ami           = "ami-0dfcb1ef8550277af"
#       instance_type = "t2.micro"
#       subnet_id     = "subnet-0ca707f38ec052efb"
#       key_name      = "jjtech-nova-keypair"
#       tags = {
#         Name = "web-app-1"
#       }
#     }
#     web-app-2 = {
#       ami           = "ami-0dfcb1ef8550277af"
#       instance_type = "t2.micro"
#       subnet_id     = "subnet-0cc283a65851ef70f"
#       key_name      = "jjtech-nova-keypair"
#       tags = {
#         Name = "web-app-2"
#       }
#     }
#   }
# }


# resource "aws_instance" "web" {
#   for_each      = var.public_app
#   ami           = each.value.ami
#   instance_type = each.value.instance_type
#   subnet_id     = each.value.subnet_id
#   key_name      = each.value.key_name
#   tags = each.value.tags
# }