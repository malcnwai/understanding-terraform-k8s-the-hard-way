output "keypair_name" {
  value = data.aws_key_pair.my_keypair.key_name
}

output "instance_id" {
  value = aws_instance.app.*.id
}

output "subnet_id" {
  value = aws_subnet.public_subnet.*.id
}