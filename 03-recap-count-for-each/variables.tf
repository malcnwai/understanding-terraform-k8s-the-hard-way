variable "region" {
  type        = string
  description = "region to launch our resources"
  default     = "us-east-1"
}

variable "vpc_cidr_practice" {
  type        = string
  description = "vpc-cidr"
  default     = "10.0.0.0/16"

}

variable "public_subnet_cidr" {
  description = "value for public subnet cidr"
  type        = list(any)
  default     = ["10.0.0.0/24", "10.0.2.0/24"]
}

variable "instance_type" {
  type        = string
  description = "instance type to launch our resources"
  default     = "t2.micro"
}

variable "key_name" {
  type        = string
  description = "name of keypair to pull down using data source"
  default     = "jjtech-nova-keypair"
}

# # variable "name_tag" {
# #   type        = string
# #   description = "name tag for instance"
# #   default     = "instance-${count.index + 1}"

# # }

# variable "public_subnet_cidr" {
#   description = "value for public subnet cidr"
#   type        = list(any)
#   default     = ["10.0.0.0/24", "10.0.2.0/24", "10.0.4.0/24", "10.0.6.0/24"]

# }

# # variable "private_subnet_cidr" {
# #   description = "value for private subnet cidr"
# #   type        = list(any)
# #   default     = ["10.0.7.0/24", "10.0.1.0/24", "10.0.3.0/24", "10.0.5.0/24"]
# # }

variable "public_subnets" {
  description = "value for public subnet ids"
  type        = list(any)
  default     = ["subnet-0dc5fa160914bcbc0", "subnet-0f9ae8c76a7b9e8be"]
}
