
################################################################
# Create 2 ec2 instances making use of count / for_each block
################################################################
# creating vpc 
resource "aws_vpc" "aws_vpc" {

  cidr_block = var.vpc_cidr_practice
  tags = {
    Name = "malc-vpc-03"
  }
} # 

locals {
  vpc_id = aws_vpc.aws_vpc.id
}

## "RESOURCE_NAME.LOCAL_NAME.DESIRED_ARTR"

resource "aws_subnet" "public_subnet" {
  count                   = length(var.public_subnet_cidr)
  vpc_id                  = local.vpc_id
  cidr_block              = var.public_subnet_cidr[count.index]
  availability_zone       = data.aws_availability_zones.available.names[count.index]
  map_public_ip_on_launch = true

  tags = {
    Name = "public_subnet-${count.index + 1}"
  }
}

# count
resource "aws_instance" "app" {
  count         = length(var.public_subnet_cidr)
  ami           = data.aws_ami.ami.id
  instance_type = var.instance_type
  key_name      = data.aws_key_pair.my_keypair.key_name
  subnet_id = var.public_subnets[count.index] 
  # #  subnet_id = element([aws_subnet.public_subnet-1.id,aws_subnet.public_subnet-2subnet_2.id], count.index)

  tags = {
    Name = "app-${count.index + 1}"
  }
}
