output "vpc_id" {
    description = "display the vpc id"
  value = aws_vpc.aws_vpc
}

output "public_ip" {
    description = "fetch the public IP"
    value = aws_instance.tf-instance_1.public_ip
}

output "subnet_id" {
    description = "display the subnet id"
    value = aws_subnet.sn-1.id
}

output "ami_id" {
    description = "display the ami id"
    value = aws_instance.tf-instance_1.ami
}

output "tag" {
    description = "display the tags"
    value = aws_instance.tf-instance_1.tags
}