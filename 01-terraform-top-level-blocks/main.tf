# Create a VPC
resource "aws_vpc" "aws_vpc" {
  cidr_block = var.vpc_cidr_block

  tags = {
    Name = "vpc"
  }
}

## Create Subnet and use data source
resource "aws_subnet" "sn-1" {
  vpc_id            = local.vpc_id
  cidr_block        = var.sn1_cidr_block
  availability_zone = data.aws_availability_zones.available.names[0]

  tags = {
    Name = "tf-subnet-1"
  }
}

resource "aws_subnet" "sn-2" {
  vpc_id            = local.vpc_id
  cidr_block        = var.sn2_cidr_block
  availability_zone = data.aws_availability_zones.available.names[1]


  tags = {
    Name = "tf-subnet-2"
  }
}

resource "aws_instance" "tf-instance_1" {
  ami           = data.aws_ami.instance_ami.id
  instance_type = "t2.micro"

  tags = {
    Name = "tf_instance_1"
  }
}
