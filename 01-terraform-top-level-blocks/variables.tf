variable "vpc_cidr_block" {
  type = string
  description = "vpc cidr block"
  default = "10.0.0.0/16"
}

variable "sn1_cidr_block" {
  description = "subnet cidr block"
  type        = string
  default     = "10.0.1.0/24"

}

variable "sn2_cidr_block" {
  description = "subnet cidr block"
  type        = string
  default     = "10.0.2.0/24"

}

variable "nat_gateway" {
  description = "value"
  type = bool
  default = true

}

variable "vpn_gateway" {
  description = "value"
  type = bool
  default = true

}

variable "module_az" {
  type = list(string)
  description = "az for module"
  default = [ "us-east-1a", "us-east-1b", "us-east-1c" ]
  
}

variable "module_public_subnets" {
  type = list(string)
  description = "public subnets for module"
  default = ["10.0.1.0/24", "10.0.2.0/24", "10.0.3.0/24"]
}

variable "module_private_subnets" {
  type = list(string)
  description = "public subnets for module"
  default = ["10.0.101.0/24", "10.0.102.0/24", "10.0.103.0/24"]
}

/*
variable "sn1_az" {
  description = "az for subnet 1"
  type        = string
  default     = "us-east-1a"

}

variable "sn2_az" {
  description = "az for subnet 2"
  type        = string
  default     = "us-east-1b"

}
*/