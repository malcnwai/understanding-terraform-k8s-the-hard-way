###
# Root Modules
####

### Private Modules / Public Modules

module "vpc" {
  source = "terraform-aws-modules/vpc/aws"

  name = "my-vpc"
  cidr = "10.0.0.0/16"

  azs             = slice(var.module_az,0,2) # use data source
  private_subnets = slice(var.module_public_subnets,0,3)
  public_subnets  = slice(var.module_private_subnets,0,3)

  enable_nat_gateway = var.nat_gateway #use variable
  enable_vpn_gateway = var.vpn_gateway #use variable

  tags = {
    Terraform = "true"
    Environment = "dev"
  }
}