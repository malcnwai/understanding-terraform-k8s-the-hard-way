
################################################################
# Create 2 ec2 instances making use of count / for_each block
################################################################
# creating vpc 
resource "aws_vpc" "aws_vpc" {

  cidr_block = var.vpc_cidr_workspace
  tags = {
    Name = "malc-vpc-05-${terraform.workspace}"
  }
} # 

locals {
  vpc_id = aws_vpc.aws_vpc.id
}

## "RESOURCE_NAME.LOCAL_NAME.DESIRED_ARTR"

resource "aws_subnet" "public_subnet_1" {
  vpc_id                  = local.vpc_id
  cidr_block              = var.public_subnet_1_cidr
  availability_zone       = "us-east-1a"
  map_public_ip_on_launch = true

  tags = {
    Name = "public_subnet-1"
  }
}

resource "aws_subnet" "public_subnet_2" {
  vpc_id                  = local.vpc_id
  cidr_block              = var.public_subnet_2_cidr
  availability_zone       = "us-east-1b"
  map_public_ip_on_launch = true

  tags = {
    Name = "public_subnet-2"
  }
}

# count
resource "aws_instance" "app" {
  count         = 2
  ami           = data.aws_ami.ami.id
  instance_type = var.instance_type
  subnet_id     = [aws_subnet.public_subnet_1.id, aws_subnet.public_subnet_2.id][count.index]
  # #  subnet_id = element([aws_subnet.public_subnet-1.id,aws_subnet.public_subnet-2subnet_2.id], count.index)

  tags = {
    Name = "app-${count.index + 1}-${terraform.workspace}"
  }
}
