terraform {
  required_version = ">=1.1.0"

  backend "s3" {
    bucket         = "workspace.state.bucket"
    key            = "path/test"
    region         = "us-east-1"
    dynamodb_table = "terraform-lock"
    encrypt        = true
  }

  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 4.0"
    }
  }
}